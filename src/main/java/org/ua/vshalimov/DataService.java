package org.ua.vshalimov;

import com.swarco.datamodel.core.kpi.*;
import net.datafaker.Faker;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDateTime;
import java.util.*;

@ApplicationScoped
public class DataService {

    private static final int COLLECTION_SIZE = 5;

    private static final Random RANDOM = new Random();

    private final Faker faker = new Faker(new Locale("de"));

    public KpiReportObjectValue getReport() {
        var report = new KpiReportObjectValue();

        report.setObjectType(KpiObjectType.TLC);
        report.setObjectName("Test TLC 1");
        report.setDay(LocalDateTime.now());
        report.setQualityType(KpiQualityType.OK);
        report.setPlanValues(getPlans(COLLECTION_SIZE));

        return report;
    }

    private List<KpiReportPlanValue> getPlans(int numberOfPlans) {
        var list = new ArrayList<KpiReportPlanValue>();

        for (int i = 0; i < numberOfPlans; i++) {
            list.add(getPlan(i + 1));
        }

        return list;
    }

    private KpiReportPlanValue getPlan(int planNumber) {
        var plan = new KpiReportPlanValue();

        plan.setPlanId(String.valueOf(planNumber));
        plan.setPlanName(String.format("Test plan %d", planNumber));
        plan.setThresholdValues(getThresholdValues(COLLECTION_SIZE));

        return plan;
    }

    private List<KpiReportThresholdValue> getThresholdValues(int numberOfThresholdValues) {
        RandomEnumGenerator<KpiThresholdType> thresholdTypeGenerator = new RandomEnumGenerator<>(KpiThresholdType.class);
        RandomEnumGenerator<KpiQualityType> qualityTypeGenerator = new RandomEnumGenerator<>(KpiQualityType.class);

        return faker.<KpiReportThresholdValue>collection()
                .suppliers(
                        () -> new KpiReportThresholdValue(
                                thresholdTypeGenerator.randomEnum(),
                                faker.number().randomDouble(2, 0, 100),
                                qualityTypeGenerator.randomEnum(),
                                getSubjectValues(7)
                        )
                )
                .minLen(numberOfThresholdValues)
                .maxLen(numberOfThresholdValues)
                .build()
                .get();
    }

    private List<KpiReportSubjectValue> getSubjectValues(int numberOfSubjectValues) {
        return faker.<KpiReportSubjectValue>collection()
                .suppliers(
                        () -> {
                            KpiReportSubjectValue subjectValue = new KpiReportSubjectValue();

                            subjectValue.setSubjectId(String.valueOf(RANDOM.nextInt()));
                            subjectValue.setTotalNumber(faker.number().numberBetween(1, 100));
                            subjectValue.setDeviationNumber(faker.number().numberBetween(1, 100));
                            subjectValue.setKpiValue(faker.number().randomDouble(2, 0, 100));
                            subjectValue.setTotalValue(faker.number().randomDouble(2, 0, 100));
                            subjectValue.setAverageValue(faker.number().randomDouble(2, 0, 100));
                            subjectValue.setMinimumValue(faker.number().randomDouble(2, 0, 100));
                            subjectValue.setMaximumValue(faker.number().randomDouble(2, 0, 100));

                            return subjectValue;
                        }
                )
                .minLen(numberOfSubjectValues)
                .maxLen(numberOfSubjectValues)
                .build()
                .get();
    }

    static class RandomEnumGenerator<T extends Enum<T>> {
        private final T[] values;

        public RandomEnumGenerator(Class<T> e) {
            values = e.getEnumConstants();
        }

        public T randomEnum() {
            return values[RANDOM.nextInt(values.length)];
        }
    }

}
