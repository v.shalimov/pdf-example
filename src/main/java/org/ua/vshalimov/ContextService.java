package org.ua.vshalimov;

import org.thymeleaf.context.Context;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@ApplicationScoped
public class ContextService {

    public <T> Context getContext(List<T> records) {
        Context context = new Context();

        context.setVariable("records", records);

        var timeCreated = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
        context.setVariable("timeCreated", timeCreated);

        return context;
    }

    public <T> Context getContext(T report) {
        var context = new Context();

        context.setVariable("report", report);

        return context;
    }

}
