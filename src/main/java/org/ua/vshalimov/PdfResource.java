package org.ua.vshalimov;

import com.swarco.pdfexport.PdfService;
import io.smallrye.mutiny.Uni;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

@Path("/pdf")
public class PdfResource {

    private final PdfService pdfService;

    private final DataService dataService;

    private final ContextService contextService;

    public PdfResource(DataService dataService, ContextService contextService) {
        pdfService = new PdfService();
        this.dataService = dataService;
        this.contextService = contextService;
    }

    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Uni<Response> hello() {
        var response = Response.ok(getBody(), MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition", "attachment;filename=" + "test.pdf")
                .build();

        return Uni.createFrom().item(response);
    }

    private byte[] getBody() {
        try {
            var template = new String(Files.readAllBytes(Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                    .getResource("templates/report.html")).toURI())));
            var context = contextService.getContext(dataService.getReport());

            return pdfService.createPdf(template, context);
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException("Error while creating PDF");
        }
    }

}