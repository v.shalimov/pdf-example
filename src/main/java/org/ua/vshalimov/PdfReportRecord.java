package org.ua.vshalimov;

public record PdfReportRecord(String firstName, String lastName, String address, double sum) {
}
